package it.lundstedt.erik.musicMenu;

import it.lundstedt.erik.util.StringTuplet;

public class Config
{
//global
	private static String $HOME=System.getProperty("HOME");

//variables
/*
artists
add as needed
artists and values should be the same except for when stage name and artist name isn't equal
 */
	private static String[] artists={"Daniel Rosenfeld","Avicii","Alan Walker","Zara Larsson","Against The current","boyinaband","abba","Alex Goot","Bruno Mars","Europe","Ac/Dc"};
	private static String[] values= {"C418" ,			"Avicii","Alan Walker","Zara Larsson","Against The current","boyinaband","abba","Alex Goot","Bruno Mars","Europe","Ac/Dc"};
	public static  StringTuplet artistNames=new StringTuplet(artists,values);
	public static String filepath = $HOME+".cache/musicMenu/";
	public static String filename = "musicMenu.sh";

	public static StringTuplet getArtistNames() {
		return artistNames;
	}
	public static int getTupletLength() {//used in main
		return artistNames.keys.length;
	}

	public static String[] getArtists() {
		return artists;
	}

	public static String[] getValues() {
		return values;
	}

}
