package it.lundstedt.erik.musicMenu;



import de.codeshelf.consoleui.prompt.ConsolePrompt;
import de.codeshelf.consoleui.prompt.ListResult;
import de.codeshelf.consoleui.prompt.PromtResultItemIF;
import de.codeshelf.consoleui.prompt.builder.ListPromptBuilder;
import de.codeshelf.consoleui.prompt.builder.PromptBuilder;
import it.lundstedt.erik.musicMenu.io.Cache;
import jline.TerminalFactory;
import org.fusesource.jansi.AnsiConsole;

import java.io.IOException;
import java.util.HashMap;

import static org.fusesource.jansi.Ansi.ansi;

public class Main {
	public static void main(String[] args) {
		AnsiConsole.systemInstall();
		//System.out.println(ansi().eraseScreen().render("@|red,italic Hello|@ @|green World|@\n@|reset|@"));
		try {
			ConsolePrompt prompt = new ConsolePrompt();
			PromptBuilder promptBuilder = prompt.getPromptBuilder();
			ListPromptBuilder modes=promptBuilder.createListPrompt();
			modes.name("modes");
			modes.message("pick a mode");
			modes.newItem("artist").text("regular").add();
			modes.newItem("unlimited-artist").text("unlimited").add();
			modes.addPrompt();
			
			//	"Daniel Rosenfeld",
			ListPromptBuilder artistMenu = promptBuilder.createListPrompt();
			artistMenu.name("artists");
			artistMenu.message("pick an artist");
			/*for (int i = 0; i <artists.length ; i++) {
				artistMenu.newItem(artists[i]).add();
			}*/
			for (int i = 0; i <Config.getTupletLength() ; i++) {
				artistMenu.newItem(Config.getArtistNames().keys[i]).text(Config.getArtistNames().values[i]).add();
			}
			
			artistMenu.addPrompt();
			
			HashMap<String, ? extends PromtResultItemIF> result = prompt.prompt(promptBuilder.build());
			//System.out.println("\n\n\n\n\n\n\n\n\n\n\n");
			System.out.println("result = " + result);
			ListResult output1 = (ListResult) result.get("modes");
			ListResult output2 = (ListResult) result.get("artists");
			String outputMode = output1.getSelectedId();
			String outputArtist = output2.getSelectedId();
			Cache cacheFile=new Cache("tizonia --gmusic-"+outputMode+" \""+outputArtist+"\"");
			cacheFile.write();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				TerminalFactory.get().restore();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
