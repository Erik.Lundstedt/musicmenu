package it.lundstedt.erik.musicMenu.io;

import it.lundstedt.erik.musicMenu.Config;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Cache
{
String command;
    public Cache(String command)
    {
        this.command = command;
    }

    public void write() throws IOException {
        String dir=Config.filepath;
        String fileName=Config.filename;
        File cacheFile=new File(dir+fileName);
        System.out.println(!cacheFile.exists());
        if (!cacheFile.exists()) {
            cacheFile.createNewFile();

        }
        if (!cacheFile.canExecute())
        {
            cacheFile.setExecutable(true,true);
        }
            FileWriter cacheWriter=new FileWriter(cacheFile);
            cacheWriter.write("#!/bin/bash\n");
            cacheWriter.append(this.command);
            cacheWriter.append("\n");
            cacheWriter.flush();
            cacheWriter.close();

    }



}
